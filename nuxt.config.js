export default {
  ssr: false,
  /*
  ** Headers of the page
  */
  head: {
    title: 'MetOSite',
    titleTemplate: '%s - MetOSite',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    noscript: [
      { innerHTML: "Your browser doesn't support JavaScript or it's disabled." }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.svg' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '@/assets/css/custom.scss'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/dateformat.js'
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    '@nuxtjs/tailwindcss'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    'bootstrap-vue/nuxt',
    // https://github.com/nuxt-community/fontawesome-module
    ['@nuxtjs/fontawesome', {
      component: 'fa',
      icons: {
        regular: [
          'faCopy',
          'faFolderOpen'
        ],
        solid: [
          'faArchive',
          'faBackward',
          'faDatabase',
          'faDownload',
          'faExternalLinkAlt',
          'faFileCsv',
          'faFilter',
          'faInfo',
          'faLongArrowAltRight',
          'faSearch',
          'faTrash', // only in alfa
          'faUpload' // only in alfa
        ]
      }
    }],
    '@nuxtjs/pwa'
  ],
  // https://bootstrap-vue.js.org/docs#nuxtjs-module
  bootstrapVue: {
    bootstrapCSS: false,
    componentPlugins: [
      'BreadcrumbPlugin',
      'ButtonToolbarPlugin', // CSS missing?
      'ButtonGroupPlugin',
      'ButtonPlugin',
      'DropdownPlugin',
      'FormCheckboxPlugin',
      'FormFilePlugin', // TODO: load only in alfa
      'FormGroupPlugin',
      'FormInputPlugin',
      'FormPlugin',
      'FormRadioPlugin',
      'LayoutPlugin',
      'NavPlugin',
      'ModalPlugin', // TODO: load only in alfa
      'PaginationPlugin',
      'ToastPlugin'
    ],
    directivePlugins: ['VBTooltipPlugin']
  },

  /* router: {
    extendRoutes (routes, resolve) {
      routes.push({
        path: '/*',
        component: resolve(__dirname, 'pages/404.vue')
      })
    }
  }, */

  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    proxy: process.env.NODE_ENV !== 'production',
    prefix: '/api/',
    https: process.env.NODE_ENV === 'production',
    debug: process.env.NODE_ENV !== 'production'
  },
  proxy: {
    '/api/': {
      target: 'http://localhost:8000/api/',
      pathRewrite: { '^/api/': '/' }
    }
  },
  pwa: {
    meta: {
      background_color: '#3994c1'
    },
    manifest: {
      display: 'standalone',
      theme_color: '#3994c1'
    },
    workbox: {
      clientsClaim: false
    }
  },
  /*
  ** Build configuration
  */
  build: {
    // You can extend webpack config here
    extend (config, ctx) {
    }
  },

  publicRuntimeConfig: {
    axios: {
      baseURL: '/api/'
    }
  }
}
