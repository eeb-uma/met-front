# MetOSite

> The oxidation of protein-bound methionine to form methionine sulfoxide (MetO), has traditionally been regarded as an oxidative damage. However, growing evidences support the view of this reversible reaction as a regulatory post-translational modification (PTM).

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:3000
npm run dev

# build for production and launch server
npm run build
npm run start

# generate static project
npm run generate

# run Storybook (UI component explorer)
npm run storybook
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
