/* eslint-disable no-console */
export const state = () => ({
  version: null,
  species: [],
  oxidants: [],
  references: [],
  count: [],
  search: {},
  lastScan: '',
  result: {},
  query: {},
  posts: [],
  perPage: 20,
  databases: {},
  FC: {
    1: 'Effect is unknown because it has not been addressed',
    2: 'No effect has been found even though it has been studied',
    3: 'Gain of activity',
    4: 'Loss of Activity',
    5: 'Gain of Protein-Protein Interaction',
    6: 'Loss of Protein-Protein Interaction',
    7: 'Effect on Protein Stability',
    8: 'Effect on Subcellular Location',
    9: 'Loss of Protein-Protein Interaction and Effect on Subcellular Location',
    10: 'Loss of Protein-Protein Interaction and Effect on Protein Stability',
    11: 'Gain of Protein-Protein Interaction and Effect on Subcellular Location',
    12: 'Gain of Protein-Protein Interaction and Effect on Protein Stability',
    13: 'Loss of Activity and Effect on Subcellular Location',
    14: 'Loss of Activity and Effect on Protein Stability',
    15: 'Gain of activity and Effect on Subcellular Location',
    16: 'Gain of activity and Effect on Protein Stability',
    17: 'Gain of Protein-Protein Interaction and Loss of Protein-Protein Interaction',
    18: 'Loss of Activity and Loss of Protein-Protein Interaction',
    19: 'Loss of Activity and Gain of Protein-Protein Interaction',
    20: 'Gain of activity and Loss of Protein-Protein Interaction',
    21: 'Gain of activity and Gain of Protein-Protein Interaction',
    22: 'Gain of activity and Loss of Activity',
    23: 'Effect on Protein Stability and Effect on Subcellular Location',
    24: 'Loss of Protein-Protein Interaction, Effect on Protein Stability and Effect on Subcellular Location',
    25: 'Gain of Protein-Protein Interaction, Effect on Protein Stability and Effect on Subcellular Location',
    26: 'Loss of Activity, Effect on Protein Stability and Effect on Subcellular Location',
    27: 'Gain of Activity, Effect on Protein Stability and Effect on Subcellular Location',
    28: 'Gain of Protein-Protein Interaction, Loss of Protein-Protein Interaction and Effect on Subcellular Location',
    29: 'Gain of Protein-Protein Interaction, Loss of Protein-Protein Interaction and Effect on Protein Stability',
    30: 'Gain of activity, Loss of Activity and Gain of Protein-Protein Interaction',
    31: 'Loss of Activity, Loss of Protein-Protein Interaction and Effect on Subcellular Location',
    32: 'Loss of Activity, Loss of Protein-Protein Interaction and Effect on Protein Stability',
    33: 'Loss of Activity, Gain of Protein-Protein Interaction and Effect on Subcellular Location',
    34: 'Loss of Activity, Gain of Protein-Protein Interaction and Effect on Protein Stability',
    35: 'Gain of activity, Loss of Protein-Protein Interaction and Effect on Subcellular Location',
    36: 'Gain of activity, Loss of Protein-Protein Interaction and Effect on Protein Stability',
    37: 'Gain of activity, Gain of Protein-Protein Interaction and Effect on Subcellular Location',
    38: 'Gain of activity, Gain of Protein-Protein Interaction and Effect on Protein Stability',
    39: 'Gain of activity, Loss of Activity and Effect on Subcellular Location',
    40: 'Gain of activity, Loss of Activity and Effect on Protein Stability',
    41: 'Loss of Activity, Gain of Protein-Protein Interaction and Loss of Protein-Protein Interaction',
    42: 'Gain of activity, Gain of Protein-Protein Interaction and Loss of Protein-Protein Interaction',
    43: 'Gain of activity, Loss of Activity and Loss of Protein-Protein Interaction',
    44: 'Gain of Protein-Protein Interaction, Loss of Protein-Protein Interaction, Effect on Protein Stability and Effect on Subcellular Location',
    45: 'Loss of Activity, Loss of Protein-Protein Interaction, Effect on Protein Stability and Effect on Subcellular Location',
    46: 'Loss of Activity, Gain of Protein-Protein Interaction, Effect on Protein Stability and Effect on Subcellular Location',
    47: 'Gain of activity, Loss of Protein-Protein Interaction, Effect on Protein Stability and Effect on Subcellular Location',
    48: 'Gain of activity, Gain of Protein-Protein Interaction, Effect on Protein Stability and Effect on Subcellular Location',
    49: 'Gain of activity, Loss of Activity, Effect on Protein Stability and Effect on Subcellular Location',
    50: 'Loss of Activity, Gain of Protein-Protein Interaction, Loss of Protein-Protein Interaction and Effect on Subcellular Location',
    51: 'Loss of Activity, Gain of Protein-Protein Interaction, Loss of Protein-Protein Interaction and Effect on Protein Stability',
    52: 'Gain of activity, Gain of Protein-Protein Interaction, Loss of Protein-Protein Interaction and Effect on Subcellular Location',
    53: 'Gain of activity, Gain of Protein-Protein Interaction, Loss of Protein-Protein Interaction and Effect on Protein Stability',
    54: 'Gain of activity, Loss of Activity, Loss of Protein-Protein Interaction and Effect on Subcellular Location',
    55: 'Gain of activity, Loss of Activity, Loss of Protein-Protein Interaction and Effect on Protein Stability',
    56: 'Gain of activity, Loss of Activity, Gain of Protein-Protein Interaction and Loss of Protein-Protein Interaction',
    57: 'Gain of activity, Loss of Activity, Gain of Protein-Protein Interaction and Effect on Subcellular Location',
    58: 'Gain of activity, Loss of Activity, Gain of Protein-Protein Interaction and Effect on Protein Stability',
    59: 'Loss of Activity, Gain of Protein-Protein Interaction, Loss of Protein-Protein Interaction, Effect on Protein Stability and Effect on Subcellular Location',
    60: 'Gain of activity, Gain of Protein-Protein Interaction, Loss of Protein-Protein Interaction, Effect on Protein Stability and Effect on Subcellular Location',
    61: 'Gain of activity, Loss of Activity, Loss of Protein-Protein Interaction, Effect on Protein Stability and Effect on Subcellular Location',
    62: 'Gain of activity, Loss of Activity, Gain of Protein-Protein Interaction, Effect on Protein Stability and Effect on Subcellular Location',
    63: 'Gain of activity, Loss of Activity, Gain of Protein-Protein Interaction, Loss of Protein-Protein Interaction and Effect on Subcellular Location',
    64: 'Gain of activity, Loss of Activity, Gain of Protein-Protein Interaction, Loss of Protein-Protein Interaction and Effect on Protein Stability',
    65: 'Gain of activity, Loss of Activity, Gain of Protein-Protein Interaction, Loss of Protein-Protein Interaction, Effect on Protein Stability and Effect on Subcellular Location'
  }
})

export const mutations = {
  SET_VERSION (state, payload) {
    state.version = payload
  },
  SET_SPECIES (state, payload) {
    state.species = payload
  },
  SET_OXIDANTS (state, payload) {
    state.oxidants = payload
  },
  SET_REFERENCES (state, payload) {
    state.references = payload
  },
  SET_COUNT (state, payload) {
    state.count = payload
  },
  SET_SEARCH (state, payload) {
    state.search = payload
  },
  SET_LAST_SCAN (state, payload) {
    state.lastScan = payload
  },
  SET_RESULT (state, payload) {
    state.result = payload
  },
  SET_QUERY (state, payload) {
    state.query = payload
  },
  SET_POSTS (state, payload) {
    state.posts = payload
  },
  SET_DATABASES (state, payload) {
    state.databases = payload
  }
}

export const actions = {
  async fetchSpecies ({ commit }) {
    try {
      const species = await this.$axios.$get('summaries/species')
      commit('SET_SPECIES', species)
    } catch (error) {
      console.error('There was a problem fetching species: ' + error.message)
    }
  },
  async fetchOxidants ({ commit }) {
    try {
      const oxidants = await this.$axios.$get('summaries/oxidant')
      commit('SET_OXIDANTS', oxidants)
    } catch (error) {
      console.error('There was a problem fetching oxidants: ' + error.message)
    }
  },
  async fetchReferences ({ commit }) {
    try {
      const references = await this.$axios.$get('summaries/references')
      commit('SET_REFERENCES', references)
    } catch (error) {
      console.error('There was a problem fetching references: ' + error.message)
    }
  },
  async fetchCount ({ commit }) {
    try {
      const count = await this.$axios.$get('summaries/count')
      commit('SET_COUNT', count)
      commit('SET_VERSION', count.last_updated)
    } catch (error) {
      console.error('There was a problem fetching count: ' + error.message)
    }
  },
  async search ({ commit }, params) {
    try {
      const filter = await this.$axios.$get('sites/search3', { params })
      commit('SET_SEARCH', filter)
    } catch (error) {
      console.error('There was a problem filtering: ' + error.message)
    }
  },
  async searchProteins ({ commit }, params) {
    try {
      const result = await this.$axios.$get('proteins/search2', { params })
      commit('SET_RESULT', result)
    } catch (error) {
      console.error('There was a problem searching proteins: ' + error.message)
    }
  },
  async scan ({ commit }, input) {
    // Uniprot RegEx, see: https://www.uniprot.org/help/accession_numbers
    const uniprotRegex = /[OPQ][0-9][A-Z0-9]{3}[0-9]|[A-NR-Z][0-9]([A-Z][A-Z0-9]{2}[0-9]){1,2}/
    if (uniprotRegex.test(input)) {
      try {
        if (input) {
          const result = await this.$axios.$get(`proteins/scan/${input}`)
          commit('SET_RESULT', result)
          commit('SET_QUERY', [])
          commit('SET_LAST_SCAN', input)
        }
      } catch (error) {
        console.error('There was a problem scanning protein: ' + error.message)
      }
    } else {
      try {
        if (input) {
          const result = await this.$axios.$get(`proteins/pname/${input}`)
          commit('SET_QUERY', result)
          commit('SET_RESULT', [])
          commit('SET_LAST_SCAN', input)
        }
      } catch (error) {
        console.error('There was a problem getting query: ' + error.message)
      }
    }
  },
  async getPosts ({ state, commit }) {
    if (state.posts.length) { return }
    try {
      let posts = await fetch('https://public-api.wordpress.com/wp/v2/sites/metositeptm.com/posts')
        .then(res => res.json())
      posts = posts
        .filter(el => el.status === 'publish')
        .map(({ id, slug, title, excerpt, date, tags, content }) => ({
          id,
          slug,
          title: title.rendered.replace('&nbsp;', ' '),
          excerpt: excerpt.rendered.substr(0, excerpt.rendered.indexOf('&hellip; <a ') + 8),
          date,
          tags,
          content
        }))
      commit('SET_POSTS', posts)
    } catch (err) {
      console.error(err)
    }
  },
  async getDatabases ({ commit }) {
    try {
      const databases = await this.$axios.$get('databases')
      commit('SET_DATABASES', databases.map((db) => { return { date: db } }))
    } catch (error) {
      console.error('There was a problem fetching the databases: ' + error.message)
    }
  },
  async loadFile ({ commit }, file) {
    try {
      const loaded = await this.$axios.$post('load', file)
      if (loaded) {
        return true
      }
      return false
    } catch (error) {
      console.error('There was a problem loading the file: ' + error.message)
    }
  },
  async deleteFile ({ commit }, file) {
    try {
      const { ok } = await this.$axios.$post('delete', file)
      if (ok) {
        this.dispatch('getDatabases')
        return true
      }
      return false
    } catch (error) {
      console.error('There was a problem deleting the file: ' + error.message)
    }
  }
}
