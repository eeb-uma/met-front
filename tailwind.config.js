/*
** TailwindCSS Configuration File
**
** Docs: https://tailwindcss.com/docs/configuration
** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
*/
module.exports = {
  theme: {
    fontFamily: {
      sans: [
        '"Source Sans Pro"', '"-apple-system"', 'BlinkMacSystemFont',
        '"Segoe UI"', 'Roboto', '"Helvetica Neue"', 'Arial', '"sans-serif"'
      ]
    },
    colors: {
      white: '#ffffff',
      black: '#000000',
      gray: {
        50: '#f5f7fa',
        100: '#e4e7eb',
        200: '#cbd2d9',
        300: '#9aa5b1',
        400: '#7b8794',
        500: '#616e7c',
        600: '#52606d',
        700: '#3e4c59',
        800: '#323f4b',
        900: '#1f2933'
      },
      blue: {
        50: '#ebf8ff',
        100: '#d1eefc',
        200: '#a7d8f0',
        300: '#7cc1e4',
        400: '#55aad4',
        500: '#3994c1',
        600: '#2d83ae',
        700: '#1d6f98',
        800: '#166086',
        900: '#0b4f71'
      },
      green: {
        50: '#e3f9e5',
        100: '#c1eac5',
        200: '#a3d9a5',
        300: '#7bc47f',
        400: '#57ae5b',
        500: '#3f9142',
        600: '#2f8132',
        700: '#207227',
        800: '#0e5814',
        900: '#05400a'
      },
      purple: {
        50: '#eae2f8',
        100: '#cfbcf2',
        200: '#a081d9',
        300: '#8662c7',
        400: '#724bb7',
        500: '#653cad',
        600: '#51279b',
        700: '#421987',
        800: '#34126f',
        900: '#240754'
      },
      yellow: {
        50: '#fffaeb',
        100: '#fcefc7',
        200: '#f8e3a3',
        300: '#f9da8b',
        400: '#f7d070',
        500: '#e9b949',
        600: '#c99a2e',
        700: '#a27c1a',
        800: '#7c5e10',
        900: '#513c06'
      }
    },
    boxShadow: {
      DEFAULT: '0 1px 2px rgba(31, 41, 51, 0.12), 0 1px 3px rgba(31, 41, 51, 0.06)',
      md: '0 2px 4px rgba(31, 41, 51, 0.06), 0 3px 6px rgba(31, 41, 51, 0.1)',
      lg: '0 3px 6px rgba(31, 41, 51, 0.05), 0 10px 20px rgba(31, 41, 51, 0.1)',
      inner: 'inset 0 1px 2px rgba(31, 41, 51, 0.1), inset 0 1px 3px rgba(31, 41, 51, 0.06)',
      focus: '0 2px 4px rgba(31, 41, 51, 0.12), 0 0 0 3px rgba(252, 239, 199, 0.5)',
      none: 'none'
    },
    listStyleType: {
      none: 'none',
      disc: 'disc',
      roman: 'lower-roman'
    },
    minWidth: {
      '1/4': '25%'
    },
    minHeight: {
      custom: '67.8vh',
      full: '100%'
    },
    cursor: {
      pointer: 'pointer',
      help: 'help'
    }
  },
  variants: {
    margin: ['responsive', 'last']
  },
  plugins: [],
  /* PurgeCSS will not detect dinamic classes like `bg-${color}-400`
     that's why we have to whitelist them here */
  purge: {
    options: {
      safelist: [
        'bg-blue-100',
        'bg-purple-400',
        'bg-yellow-100',
        'border-blue-500',
        'border-green-500',
        'border-yellow-500',
        'hover:bg-purple-500',
        'text-blue-900',
        'text-purple-700',
        'text-yellow-900',
      ]
    }
  }
}
